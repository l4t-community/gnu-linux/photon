#include "bluetooth.hpp"

#define BT_ADDRESS_STRING_SIZE 18

bool BlueZ::state()
{
	const char *state_buf;
	bool state = true;

	const gchar *old_bus = dbus.busName;
	const gchar *old_path = dbus.objectPath;

	dbus.busName = "org.freedesktop.systemd1";
	dbus.objectPath = "/org/freedesktop/systemd1/unit/bluetooth_2etarget";

	state_buf = dbus.getStrProp("org.freedesktop.systemd1.Unit", "ActiveState");

	if (strcmp(state_buf, "inactive") == 0)
		state = false;

	dbus.busName = old_bus;
	dbus.objectPath = old_path;

	return state;
};

bool BlueZ::enable()
{
	return dbus.setProp("org.bluez.Adapter1", "Powered", g_variant_new("b", TRUE));
};

bool BlueZ::disable()
{
	return dbus.setProp("org.bluez.Adapter1", "Powered", g_variant_new("b", FALSE));
};

bool BlueZ::pair(const char *device)
{
	dbus.objectPath = device;
	dbus.interface = "org.bluez.Device1";
	return dbus.genericMethodCall("Pair", g_variant_new("o", device));
};

bool BlueZ::unpair(const char *device)
{
	dbus.objectPath = device;
	dbus.interface = "org.bluez.Device1";
	return dbus.genericMethodCall("CancelPairing", g_variant_new("o", device));
};

bool BlueZ::connect(const char *device)
{
	dbus.objectPath = device;
	dbus.interface = "org.bluez.Device1";
	return dbus.genericMethodCall("Connect", g_variant_new("o", device));
};

bool BlueZ::disconnect(const char *device)
{
	dbus.objectPath = device;
	dbus.interface = "org.bluez.Device1";
	return dbus.genericMethodCall("Disconnect", g_variant_new("o", device));
};

bool BlueZ::remove(const char *device)
{
	dbus.interface = "org.bluez.Adapter1";
	return dbus.genericMethodCall("RemoveDevice", g_variant_new("o", device));
};

bool BlueZ::start_discovery()
{
	dbus.interface = "org.bluez.Adapter1";
	return dbus.genericMethodCall("StartDiscovery", NULL);
};

bool BlueZ::stop_discovery()
{
	dbus.interface = "org.bluez.Adapter1";
	return dbus.genericMethodCall("StopDiscovery", NULL);
};

std::vector<std::string> BlueZ::list_devices()
{
	GVariant *objects;
	GVariantIter iter;
	const gchar *object_path;
	GVariant *ifaces_and_properties;
	std::vector<std::string> devices;

	dbus.objectPath = "/";
	dbus.interface = "org.freedesktop.DBus.ObjectManager";

	objects = dbus.methodCall("GetManagedObjects", NULL);
	objects = g_variant_get_child_value(objects, 0);
	g_variant_iter_init(&iter, objects);

	while (g_variant_iter_next(&iter, "{&o@a{sa{sv}}}", &object_path, &ifaces_and_properties))
	{

		const gchar *interface_name;
		GVariant *properties;
		GVariantIter ii;

		g_variant_iter_init(&ii, ifaces_and_properties);

		while (g_variant_iter_next(&ii, "{&s@a{sv}}", &interface_name, &properties))
		{

			if (g_strstr_len(g_ascii_strdown(interface_name, -1), -1, "device"))
				devices.push_back(object_path);

			g_variant_unref(properties);
		}

		g_variant_unref(ifaces_and_properties);
	}

	g_variant_unref(objects);

	return devices;
};

void BlueZ::bluez_property_value(const gchar *key, GVariant *value)
{
	const gchar *type = g_variant_get_type_string(value);

	g_print("\t%s : ", key);
	switch (*type)
	{
	case 'o':
	case 's':
		g_print("%s\n", g_variant_get_string(value, NULL));
		break;
	case 'b':
		g_print("%d\n", g_variant_get_boolean(value));
		break;
	case 'u':
		g_print("%d\n", g_variant_get_uint32(value));
		break;
	case 'a':
		/* TODO Handling only 'as', but not array of dicts */
		if (g_strcmp0(type, "as"))
			break;
		g_print("\n");
		const gchar *uuid;
		GVariantIter i;
		g_variant_iter_init(&i, value);
		while (g_variant_iter_next(&i, "s", &uuid))
			g_print("\t\t%s\n", uuid);
		break;
	default:
		g_print("Other\n");
		break;
	}
}

int BlueZ::bluez_adapter_call_method(const char *method, GVariant *param)
{
	GVariant *result;

	dbus.objectPath = "/org/bluez/hci0";
	dbus.interface = "org.bluez.Adapter1";
	result = dbus.methodCall(method, param);

	g_variant_unref(result);
	return 0;
}

void BlueZ::bluez_device_appeared(GDBusConnection *sig,
								  const gchar *sender_name,
								  const gchar *object_path,
								  const gchar *interface,
								  const gchar *signal_name,
								  GVariant *parameters,
								  gpointer user_data)
{
	(void)sig;
	(void)sender_name;
	(void)object_path;
	(void)interface;
	(void)signal_name;
	(void)user_data;

	GVariantIter *interfaces;
	const char *object;
	const gchar *interface_name;
	GVariant *properties;
	int rc;

	g_variant_get(parameters, "(&oa{sa{sv}})", &object, &interfaces);
	while (g_variant_iter_next(interfaces, "{&s@a{sv}}", &interface_name, &properties))
	{
		if (g_strstr_len(g_ascii_strdown(interface_name, -1), -1, "device"))
		{
			g_print("[ %s ]\n", object);
			const gchar *property_name;
			GVariantIter i;
			GVariant *prop_val;
			g_variant_iter_init(&i, properties);
			while (g_variant_iter_next(&i, "{&sv}", &property_name, &prop_val))
				bluez_property_value(property_name, prop_val);
			g_variant_unref(prop_val);
		}
		g_variant_unref(properties);
	}
	rc = bluez_adapter_call_method("RemoveDevice", g_variant_new("(o)", object));
	if (rc)
		g_print("Not able to remove %s\n", object);
	return;
}

void BlueZ::bluez_device_disappeared(GDBusConnection *sig,
									 const gchar *sender_name,
									 const gchar *object_path,
									 const gchar *interface,
									 const gchar *signal_name,
									 GVariant *parameters,
									 gpointer user_data)
{
	(void)sig;
	(void)sender_name;
	(void)object_path;
	(void)interface;
	(void)signal_name;

	GVariantIter *interfaces;
	const char *object;
	const gchar *interface_name;
	char address[BT_ADDRESS_STRING_SIZE] = {'\0'};

	g_variant_get(parameters, "(&oas)", &object, &interfaces);
	while (g_variant_iter_next(interfaces, "s", &interface_name))
	{
		if (g_strstr_len(g_ascii_strdown(interface_name, -1), -1, "device"))
		{
			int i;
			char *tmp = g_strstr_len(object, -1, "dev_") + 4;

			for (i = 0; *tmp != '\0'; i++, tmp++)
			{
				if (*tmp == '_')
				{
					address[i] = ':';
					continue;
				}
				address[i] = *tmp;
			}
			g_print("\nDevice %s removed\n", address);
			g_main_loop_quit((GMainLoop *)user_data);
		}
	}
	return;
}

void BlueZ::bluez_signal_adapter_changed(GDBusConnection *conn,
										 const gchar *sender,
										 const gchar *path,
										 const gchar *interface,
										 const gchar *signal,
										 GVariant *params,
										 void *userdata)
{
	(void)conn;
	(void)sender;
	(void)path;
	(void)interface;
	(void)userdata;

	GVariantIter *properties = NULL;
	GVariantIter *unknown = NULL;
	const char *iface;
	const char *key;
	GVariant *value = NULL;
	const gchar *signature = g_variant_get_type_string(params);

	if (strcmp(signature, "(sa{sv}as)") != 0)
	{
		g_print("Invalid signature for %s: %s != %s", signal, signature, "(sa{sv}as)");
		goto done;
	}

	g_variant_get(params, "(&sa{sv}as)", &iface, &properties, &unknown);
	while (g_variant_iter_next(properties, "{&sv}", &key, &value))
	{
		if (!g_strcmp0(key, "Powered"))
		{
			if (!g_variant_is_of_type(value, G_VARIANT_TYPE_BOOLEAN))
			{
				g_print("Invalid argument type for %s: %s != %s", key,
						g_variant_get_type_string(value), "b");
				goto done;
			}
			g_print("Adapter is Powered \"%s\"\n", g_variant_get_boolean(value) ? "on" : "off");
		}
		if (!g_strcmp0(key, "Discovering"))
		{
			if (!g_variant_is_of_type(value, G_VARIANT_TYPE_BOOLEAN))
			{
				g_print("Invalid argument type for %s: %s != %s", key,
						g_variant_get_type_string(value), "b");
				goto done;
			}
			g_print("Adapter scan \"%s\"\n", g_variant_get_boolean(value) ? "on" : "off");
		}
	}
done:
	if (properties != NULL)
		g_variant_iter_free(properties);
	if (value != NULL)
		g_variant_unref(value);
}

bool BlueZ::bluez_adapter_set_property(const char *prop, GVariant *value)
{
	dbus.objectPath = "/org/bluez/hci0";
	dbus.interface = "org.freedesktop.DBus.Properties";
	return dbus.setProp("org.bluez.Adapter1", prop, value);
}

// int main(void)
// {
// 	GMainLoop *loop;
// 	int rc;
// 	guint prop_changed;
// 	guint iface_added;
// 	guint iface_removed;

// 	con = g_bus_get_sync(G_BUS_TYPE_SYSTEM, NULL, NULL);
// 	if(con == NULL) {
// 		g_print("Not able to get connection to system bus\n");
// 		return 1;
// 	}

// 	loop = g_main_loop_new(NULL, FALSE);

// 	prop_changed = g_dbus_connection_signal_subscribe(con,
// 						"org.bluez",
// 						"org.freedesktop.DBus.Properties",
// 						"PropertiesChanged",
// 						NULL,
// 						"org.bluez.Adapter1",
// 						G_DBUS_SIGNAL_FLAGS_NONE,
// 						bluez_signal_adapter_changed,
// 						NULL,
// 						NULL);

// 	iface_added = g_dbus_connection_signal_subscribe(con,
// 							"org.bluez",
// 							"org.freedesktop.DBus.ObjectManager",
// 							"InterfacesAdded",
// 							NULL,
// 							NULL,
// 							G_DBUS_SIGNAL_FLAGS_NONE,
// 							bluez_device_appeared,
// 							loop,
// 							NULL);

// 	iface_removed = g_dbus_connection_signal_subscribe(con,
// 							"org.bluez",
// 							"org.freedesktop.DBus.ObjectManager",
// 							"InterfacesRemoved",
// 							NULL,
// 							NULL,
// 							G_DBUS_SIGNAL_FLAGS_NONE,
// 							bluez_device_disappeared,
// 							loop,
// 							NULL);

// 	BlueZ::enable();

// 	BlueZ::start_discovery();

// 	g_main_loop_run(loop);

// 	BlueZ::stop_discovery();

// 	g_usleep(100);

// fail:
// 	g_dbus_connection_signal_unsubscribe(con, prop_changed);
// 	g_dbus_connection_signal_unsubscribe(con, iface_added);
// 	g_dbus_connection_signal_unsubscribe(con, iface_removed);
// 	g_object_unref(con);
// 	return 0;
// }
