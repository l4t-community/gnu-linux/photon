#include "dbus.hpp"

bool DBus::setProp(const char *adapter, const char *prop, GVariant *value)
{
    GVariant *call = methodCall("Set", g_variant_new("(ssv)", adapter, prop, value));
    g_variant_unref(call);
    return true;
}

GVariant *DBus::getProp(const gchar *adapter, const char *prop)
{
    GVariant *ret;
    GVariant *call = methodCall("org.freedesktop.DBus.Properties.Get",
        g_variant_new("(ss)", adapter, prop));

    g_variant_get(call, "(v)", &ret);
    g_variant_unref(call);

    return ret;
}

const gchar *DBus::getStrProp(const gchar *adapter, const char *prop)
{
    const gchar *ret;
    GVariant *call = getProp(adapter, prop);
    
    g_variant_get(call, "s", &ret);
    g_variant_unref(call);

    return ret;
}

bool DBus::getBoolProp(const gchar *adapter, const char *prop)
{
    bool ret;
    GVariant *call = getProp(adapter, prop);

    g_variant_get(call, "b", &ret);
    g_variant_unref(call);

    return ret;
}

int DBus::getIntProp(const gchar *adapter, const char *prop)
{
    bool ret;
    GVariant *call = getProp(adapter, prop);
    
    g_variant_get(call, "i", &ret);
    g_variant_unref(call);

    return ret;
}

gchar *DBus::findDevice(const gchar *pattern)
{
    GVariantIter *iter;
    gchar *name, *res = NULL;
    GVariant *call = methodCall("EnumerateDevices", NULL);

    g_variant_get(call, "(ao)", &iter);
    while (g_variant_iter_loop(iter, "o", &name))
    {
        if (strcasestr(name, pattern))
        {
            res = name;
            break;
        }
    }

    g_variant_iter_free(iter);
    g_variant_unref(call);

    return res;
}

bool DBus::genericMethodCall(const char *method, GVariant *param = NULL)
{
    GVariant *call = methodCall(method, param);
    g_variant_unref(call);
    return true;
}

GDBusProxy *DBus::createProxy()
{
    GError *error = NULL;
    GDBusProxy *proxy = g_dbus_proxy_new_for_bus_sync(
        busType,
        G_DBUS_PROXY_FLAGS_NONE,
        NULL,
        busName,
        objectPath,
        interface,
        NULL,
        &error);

    g_assert(proxy);
    return proxy;
}

GVariant *DBus::methodCall(const char *method, GVariant *param = NULL)
{
    GVariant *call;
    GError *error = NULL;
    GDBusProxy *proxy = createProxy();

    call = g_dbus_proxy_call_sync(
        proxy,
        method,
        param,
        G_DBUS_CALL_FLAGS_NONE,
        -1,
        NULL,
        &error);

    if (!call)
    {
        g_dbus_error_strip_remote_error(error);
        g_warning("Failed to get property: %s\n", error->message);
        g_error_free(error);
        return NULL;
    }

    return call;
}
