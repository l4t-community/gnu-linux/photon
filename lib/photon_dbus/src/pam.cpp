#include "pam.hpp"

bool PAM::create(const char *name, const char *fullname, int accType)
{
    dbus.genericMethodCall("CreateUser", g_variant_new("(ssi)", name, fullname, accType));
    return dbus.genericMethodCall("CacheUser", g_variant_new("(ssi)", name));
};

bool PAM::remove(int id, bool rmFiles)
{
    return dbus.genericMethodCall("DeleteUser", g_variant_new("(ib)", id, rmFiles));
};

std::vector<std::string> PAM::listUsers()
{
    dbus.methodCall("ListCachedUsers", g_variant_new("(ao)"));
    return {};
};
