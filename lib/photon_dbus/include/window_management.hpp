#include "dbus.hpp"

#pragma once

#define BUS_NAME ""
#define BUS_PATH ""
#define BUS_IFACE ""

class IWindowManager
{
public:
    // Init window manager
    virtual bool init() = 0;

    // Deinit window manager
    virtual bool deinit() = 0;

    // Open a new window
    virtual bool open() = 0;

    // Kill a window
    virtual bool kill() = 0;

    // Hide window
    virtual bool hide() = 0;
};