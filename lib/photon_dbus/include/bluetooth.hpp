#pragma once
#include "dbus.hpp"
#include <iostream>
#include <vector>
#include <string>

#define BUS_NAME "org.bluez"
#define BUS_PATH "/org/bluez/hci0"
#define BUS_IFACE "org.freedesktop.DBus.Properties"

class IBluetooth
{
public:
    // Get state
    virtual bool state() = 0;

    // Enable adapter
    virtual bool enable() = 0;

    // Disable adapter
    virtual bool disable() = 0;

    // List known devices
    virtual std::vector<std::string> list_devices() = 0;

    // Pair device
    virtual bool pair(const char *device) = 0;

    // Unpair device
    virtual bool unpair(const char *device) = 0;

    // Connect to device
    virtual bool connect(const char *device) = 0;

    // Disonnect from device
    virtual bool disconnect(const char *device) = 0;

    // Remove device
    virtual bool remove(const char *device) = 0;
};

class BlueZ : public IBluetooth
{
public:
    DBus dbus;

    BlueZ() : dbus(BUS_NAME, BUS_PATH, BUS_IFACE){};

    bool state();

    bool enable();

    bool disable();

    bool pair(const char *device);

    bool unpair(const char *device);

    bool connect(const char *device);

    bool disconnect(const char *device);

    bool remove(const char *device);

    bool start_discovery();

    bool stop_discovery();

    std::vector<std::string> list_devices();

    void bluez_property_value(const gchar *key, GVariant *value);

    int bluez_adapter_call_method(const char *method, GVariant *param);

    void bluez_device_appeared(GDBusConnection *sig,
                               const gchar *sender_name,
                               const gchar *object_path,
                               const gchar *interface,
                               const gchar *signal_name,
                               GVariant *parameters,
                               gpointer user_data);

    void bluez_device_disappeared(GDBusConnection *sig,
                                  const gchar *sender_name,
                                  const gchar *object_path,
                                  const gchar *interface,
                                  const gchar *signal_name,
                                  GVariant *parameters,
                                  gpointer user_data);

    void bluez_signal_adapter_changed(GDBusConnection *conn,
                                      const gchar *sender,
                                      const gchar *path,
                                      const gchar *interface,
                                      const gchar *signal,
                                      GVariant *params,
                                      void *userdata);

    bool bluez_adapter_set_property(const char *prop, GVariant *value);
};