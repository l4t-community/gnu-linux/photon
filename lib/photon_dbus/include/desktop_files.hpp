#include <vector>
#include <gio-unix-2.0/gio/gdesktopappinfo.h>
#include <gtk-3.0/gtk/gtk.h>

#pragma once

typedef struct DesktopFilesList
{
    const char *name;
    const char *iconPath;
    GAppInfo *ref;
};

class DesktopFiles
{
public:
    // Fetch all .desktop entries
    static std::vector<DesktopFilesList> fetch();

    // Launch app callback
    static bool launch(GAppInfo *app);
};