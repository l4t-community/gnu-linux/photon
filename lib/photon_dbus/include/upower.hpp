#ifndef __DBUS_UPOWER__
#define __DBUS_UPOWER__

#include "dbus.hpp"

#define BUS_NAME "org.freedesktop.UPower"
#define BUS_PATH "/org/freedesktop/UPower/devices/DisplayDevice"
#define BUS_IFACE "org.freedesktop.DBus.Properties"

class IPowerManagement
{
    public:
        virtual int percentage() = 0;
};

class UPower : public IPowerManagement
{
    public:
        DBus dbus;

        UPower() : dbus(BUS_NAME, BUS_PATH, BUS_IFACE){};

        bool isOnBattery();
        const gchar *get_battery_path();
        int percentage();
};

#endif
