#include "dbus.hpp"

#pragma once

#define BUS_NAME ""
#define BUS_PATH ""
#define BUS_IFACE ""

class IWireless
{
public:
    virtual ~IWireless(){};

    // Enable card
    virtual bool enable() = 0;

    // Disable card
    virtual bool disable() = 0;

    // Get card status
    virtual bool getStatus() = 0;

    // List Access Points
    virtual char *listAccessPoints() = 0;

    // Create AP
    virtual bool create() = 0;

    // Connect to AP
    virtual bool connect() = 0;

    // Disonnect from AP
    virtual bool disconnect() = 0;

    // Remove AP
    virtual bool remove() = 0;
};

class Connman : public IWireless
{
private:
    DBus dbus;

public:
    virtual ~Connman(){};

    Connman() : dbus(BUS_NAME, BUS_PATH, BUS_IFACE){};

    bool enable();

    bool disable();

    char *listAccessPoints();

    bool create();

    bool connect();

    bool disconnect();

    bool remove();
};
