#include "dbus.hpp"
#include <iostream>
#include <vector>
#include <string>

#pragma once

#define BUS_NAME "org.freedesktop.Accounts"
#define BUS_PATH "/org/freedesktop/Accounts"
#define BUS_IFACE "org.freedesktop.Accounts"

class IAccount
{
public:
    // Create new user
    virtual bool create(const char *name, const char *fullname, int accType) = 0;

    // Remove user
    virtual bool remove(int id, bool rmFiles) = 0;

    // Modify user
    // virtual bool edit() = 0;

    // Show user info
    // static int infos();

    // List cached users
    virtual std::vector<std::string> listUsers() = 0;
};

class PAM : public IAccount
{
public:
    DBus dbus;

    PAM() : dbus(BUS_NAME, BUS_PATH, BUS_IFACE){};

    bool create(const char *name, const char *fullname, int accType);

    bool remove(int id, bool rmFiles);

    std::vector<std::string> listUsers();
};