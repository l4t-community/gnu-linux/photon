#pragma once
#include "dbus.hpp"

#define BUS_NAME    "org.gnome.SettingsDaemon.Power"
#define BUS_PATH    "/org/gnome/SettingsDaemon/Power"
#define BUS_IFACE   "org.gnome.SettingsDaemon.Power.Screen"
#define BUS_TYPE    G_BUS_TYPE_SESSION

class Brightness
{
    public:
        DBus dbus;

        Brightness() : dbus(BUS_NAME, BUS_PATH, BUS_IFACE, BUS_TYPE){};

        bool brightnessUp();
        bool brightnessDown();
        int read();
};