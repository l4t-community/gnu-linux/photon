/*
    Photon, a Nintendo Switch UI
    Copyright (C) 2021-2022  Azkali Manad

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <borealis.hpp>
#include <gtk-3.0/gtk/gtk.h>

#include "home/home.hpp"
#include "home/carousel.hpp"
#include "home/drawer.hpp"
#include "home/navbar.hpp"

#include "initialSetup/initialSetup.hpp"

#include "settings/settings.hpp"
#include "settings/list.hpp"
#include "settings/powerTab.hpp"
#include "settings/wirelessTab.hpp"

using namespace brls::literals; // for _i18n

int main(int argc, char *argv[])
{
	brls::Logger::setLogLevel(brls::LogLevel::DEBUG);

	if (!brls::Application::init())
	{
		brls::Logger::error("Unable to init Borealis application");
		return EXIT_FAILURE;
	}

	gtk_init(&argc, &argv);
	brls::Application::createWindow("Photon"_i18n);
	brls::Application::setGlobalQuit(true);

	brls::getLightTheme().addColor("app_box/label", nvgRGB(2, 176, 183));
	brls::getDarkTheme().addColor("app_box/label", nvgRGB(51, 186, 227));

	brls::getLightTheme().addColor("photon/separator", nvgRGB(0xcd, 0xcd, 0xcd));
	brls::getDarkTheme().addColor("photon/separator", nvgRGB(0x4e, 0x4e, 0x4e));

	brls::Application::registerXMLView("photon:AppCell", photon::views::AppCell::create);
	brls::Application::registerXMLView("photon:Carousel", photon::views::Carousel::create);
	brls::Application::registerXMLView("photon:List", photon::views::List::create);
	brls::Application::registerXMLView("photon:NavBar", photon::views::NavBar::create);
	brls::Application::registerXMLView("photon:Drawer", photon::views::Drawer::create);

	brls::Application::registerXMLView("WirelessTab", photon::tabs::WirelessTab::create);
	brls::Application::registerXMLView("PowerTab", photon::tabs::PowerTab::create);

	bool initialSetup = false;
	if (initialSetup)
		brls::Application::pushActivity(new photon::activities::InitialSetup());
	else
		brls::Application::pushActivity(new photon::activities::Home());

	while (brls::Application::mainLoop())
		;

	return EXIT_SUCCESS;
}
