#include "utils/osk.hpp"

namespace photon::utils {
    ActionIdentifier registerAction(std::string hintText,
        brls::BrlsKeyboardScancode button,
        ActionListener actionListener,
        bool allowRepeating,
        enum brls::Sound sound)
    {
        ActionIdentifier nextIdentifier = (this->actions.size() == 0) ? 1 : this->actions.back().identifier + 1;

        if (auto it = std::find(this->actions.begin(), this->actions.end(), button); it != this->actions.end())
            *it = { button, nextIdentifier, hintText, true, allowRepeating, sound, actionListener };
        else
            this->actions.push_back({ button, nextIdentifier, hintText, true, allowRepeating, sound, actionListener });

        return nextIdentifier;
    }

    Key::Key(std::string name,
            brls::BrlsKeyboardScancode keycode,
            float sizeHint = 1.0f)
    {
        this->setText(name);
        this->setDimensions(sizeHint, 1.0f);
    }

    brls::View *Key::create()
    {
        return new Key();
    }

    Osk::Osk()
    {
        // Row 1
        brls::Box *firstRow = new brls::Box();
        Key("ESC", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_ESCAPE);
        Key("q", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_Q);
        Key("w", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_W);
        Key("e", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_E);
        Key("r", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_R);
        Key("t", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_T);
        Key("y", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_Y);
        Key("u", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_U);
        Key("i", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_I);
        Key("o", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_O);
        Key("p", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_P);
        Key("←", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_BACKSPACE, 2.0f);

        // Row 1 CAPS
        Key("Q", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_Q);
        Key("W", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_W);
        Key("E", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_E);
        Key("R", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_R);
        Key("T", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_T);
        Key("Y", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_Y);
        Key("U", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_U);
        Key("I", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_I);
        Key("O", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_O);
        Key("P", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_P);

        // Row 2
        brls::Box *secondRow = new brls::Box();
        Key("TAB", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_TAB, 1.5f);
        Key("a", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_A);
        Key("s", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_S);
        Key("d", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_D);
        Key("f", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_F);
        Key("g", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_G);
        Key("h", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_H);
        Key("j", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_J);
        Key("k", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_K);
        Key("l", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_L);
        Key("'", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_APOSTROPHE);
        Key("→", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_ENTER, 1.5f);

        // Row 3
        brls::Box *thirdRow = new brls::Box();
        Key("SHIFT", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_LEFT_SHIFT, 2.0f);
        Key("z", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_Z);
        Key("x", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_X);
        Key("c", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_C);
        Key("v", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_V);
        Key("b", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_B);
        Key("n", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_N);
        Key("m", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_M);
        Key(",", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_COMMA);
        Key(".", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_PERIOD);
        Key("?", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_APOSTROPHE);
        Key("SHIFT", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_RIGHT_SHIFT);

        // Row 4
        brls::Box *fourthRow = new brls::Box();
        // Change layout special key
        Key("CTRL", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_LEFT_CONTROL);
        Key("SUPER", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_LEFT_SUPER);
        Key("ALT", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_LEFT_ALT);
        Key("/", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_SLASH);
        Key("SPACE", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_SPACE, 4.0f);
        Key("<", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_LEFT);
        Key("", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_DOWN);
        Key(">", brls::BrlsKeyboardScancode::BRLS_KBD_KEY_RIGHT);

        brls::Box *keyboard = new brls::Box();
        keyboard->setMinWidthPercentage(100);
        keyboard->setMaxHeightPercentage(45);

        firstRow->setWidthPercentage(90);
        firstRow->setHeightPercentage(25);
        keyboard->addView(firstRow);

        secondRow->setWidthPercentage(90);
        secondRow->setHeightPercentage(25);
        keyboard->addView(secondRow);

        thirdRow->setWidthPercentage(90);
        thirdRow->setHeightPercentage(25);
        keyboard->addView(thirdRow);

        fourthRow->setWidthPercentage(90);
        fourthRow->setHeightPercentage(25);
        keyboard->addView(fourthRow);

        this->addView(keyboard);
        this->setVisibility(brls::Visibility::GONE);
    }

    void Osk::showOsk(brls::View *view)
    {
        this->setVisibility(brls::Visibility::VISIBLE);
    }

    brls::View *Osk::create()
    {
        return new Osk();
    }
}