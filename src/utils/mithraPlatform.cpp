#include "utils/mithraPlatform.hpp"

namespace photon::platform
{
    MithraPlatform::MithraPlatform() {}

    int MithraPlatform::getBatteryLevel()
    {
        return upower.percentage();
    }

    bool MithraPlatform::hasWirelessConnection()
    {
        return false;
    }
}