#include <string>

#include "settings/wirelessTab.hpp"
#include "wireless.hpp"

const std::string wirelessTab = R"xml(
<brls:Box
  width="auto"
  height="auto">

  <brls:ScrollingFrame
    width="auto"
    height="auto"
    grow="1.0" >

    <brls:Box
      width="auto"
      height="auto"
      axis="column"
      grow="1.0"
      paddingLeft="@style/brls/tab_frame/content_padding_sides"
      paddingRight="@style/brls/tab_frame/content_padding_sides"
      paddingTop="@style/brls/tab_frame/content_padding_top_bottom"
      paddingBottom="@style/brls/tab_frame/content_padding_top_bottom">

      <photon:List
        id="wifi_button"
        imageWidth="30"
        imageHeight="30"
        grow="1.0"
        text="Wi-Fi"
        image="@res/icon/wifi_dark.png"/>

      <photon:List
        id="bluetooth_button"
        imageHeight="30"
        imageWidth="30"
        grow="1.0"
        text="Bluetooth"
        image="@res/icon/bluetooth_dark.png"/>

    </brls:Box>
  </brls:ScrollingFrame>
</brls:Box>
)xml";

namespace photon::tabs
{
    /* Constructs the settings activity wireless tab */
    WirelessTab::WirelessTab()
    {
        this->inflateFromXMLString(wirelessTab);
        BRLS_REGISTER_CLICK_BY_ID("wifi_button", this->onWifiButtonClicked);
        BRLS_REGISTER_CLICK_BY_ID("bluetooth_button", this->onBluetoothButtonClicked);
    }

    /** Set bluetooth settings click action
     * @param view The currrent view 
     * @return Enable/Disable bluetooth
     */
    bool WirelessTab::onBluetoothButtonClicked(brls::View* view)
    {
        return bluez.state() ? bluez.disable() : bluez.enable();
    }

    /** Set WiFi settings click action
     * @param view The current view
     * @return Enable/Disable WiFi
     */
    bool WirelessTab::onWifiButtonClicked(brls::View* view)
    {
        return true;
    }

    /** List available bluetooth devices
     * @return A list of BT devices
     */
    std::vector<std::string> WirelessTab::ListBluetoothDevices()
    {
        std::string device;
        std::vector<std::string> devices = bluez.list_devices();

        for (int i = 0; i <= (sizeof(devices)/sizeof(devices[0])); i++)
            device = "Address: " + devices[i];

        return {};
    }

    /** List available access points
     * @return A list of available access points
     */
    std::vector<std::string> WirelessTab::ListAccessPoints()
    {
        return {};
    }

    brls::View *WirelessTab::create()
    {
        return new WirelessTab();
    }
}