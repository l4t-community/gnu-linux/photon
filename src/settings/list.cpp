#include <string>

#include "settings/list.hpp"

const std::string list = R"xml(
<brls:Box
  width="auto"
  height="auto"
  grow="1.0"
  axis="column">

  <brls:Button
    grow="1.0"
    style="borderless">

    <brls:Box
      width="auto"
      height="auto"
      axis="row"
      marginTop="10"
      grow="1.0">

      <brls:Image
        focusable="true"
        id="image"
        marginRight="20px" />

      <brls:Label
        id="label"
        width="auto"
        height="100%"
        grow="1.0"
        marginBottom="5px"
        fontSize="25" />

      <brls:Label
        id="selected_item"
        text="works"
        horizontalAlign="right"
        marginRight="5px"
        textColor="#58c2a8"
        fontSize="20"
        width="auto"
        height="100%"
        grow="1.0"/>

    </brls:Box>

  </brls:Button>

  <brls:Rectangle
    width="100%"
    height="1"
    color="@theme/photon/separator" />

</brls:Box>
)xml";

namespace photon::views
{
    List::List()
    {
        this->inflateFromXMLString(list);

        this->forwardXMLAttribute("image", this->image);
        this->forwardXMLAttribute("focusUp", this->image);
        this->forwardXMLAttribute("focusRight", this->image);
        this->forwardXMLAttribute("focusDown", this->image);
        this->forwardXMLAttribute("focusLeft", this->image);
        this->forwardXMLAttribute("imageWidth", this->image, "width");
        this->forwardXMLAttribute("imageHeight", this->image, "height");

        this->forwardXMLAttribute("text", this->label);
        this->forwardXMLAttribute("fontSize", this->label);
    }

    brls::View *List::create()
    {
        return new List();
    }
}