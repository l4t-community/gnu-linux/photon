#include "settings/powerTab.hpp"

const std::string powerTab = R"xml(
<brls:Box width="auto" height="auto">
    <brls:ScrollingFrame width="auto" height="auto" grow="1.0">

        <brls:Box
            width="auto"
            height="auto"
            axis="column"
            grow="1.0"
            paddingLeft="@style/brls/tab_frame/content_padding_sides"
            paddingRight="@style/brls/tab_frame/content_padding_sides"
            paddingTop="@style/brls/tab_frame/content_padding_top_bottom"
            paddingBottom="@style/brls/tab_frame/content_padding_top_bottom">

            <brls:Header width="auto" height="auto" title="Brightness" marginBottom="15px"/>

            <brls:Box width="auto" height="auto" axis="row">
                <brls:Label
                    id="progress"
                    width="80"
                    height="40"
                    marginBottom="10px"
                    horizontalAlign="center"
                    verticalAlign="center"/>
                <brls:Slider id="brightness" grow="1.0"/>
            </brls:Box>

            <photon:List
                imageHeight="30"
                imageWidth="30"
                grow="1.0"
                text="Screen timeout"
                image="@res/icon/brightness_dark.png"/>

            <photon:List
                imageHeight="30"
                imageWidth="30"
                grow="1.0"
                text="CPU clock"
                image="@res/icon/power_dark.png"/>

            <photon:List
                imageHeight="30"
                imageWidth="30"
                grow="1.0"
                text="GPU clock"
                image="@res/icon/power_dark.png"/>

        </brls:Box>
    </brls:ScrollingFrame>
</brls:Box>
)xml";

namespace photon::tabs
{
    /* Constructs the Settings power tab */
    PowerTab::PowerTab()
    {
        this->inflateFromXMLString(powerTab);
        setBrightnessLevel();
    }

    /* Set slider brightness level */
    void PowerTab::setBrightnessLevel()
    {
        int level = readBrightness() / 2.55f;

        progress->setText(std::to_string(level));
        slider->setProgress(level);

        slider->registerAction(
        "Left Click", brls::ControllerButton::BUTTON_NAV_LEFT, [this](View* view) {
            return brightnessDown();
        },
        true, true, brls::Sound::SOUND_NONE);

        slider->registerAction(
        "Right Click", brls::ControllerButton::BUTTON_NAV_RIGHT, [this](View* view) {
            return brightnessUp();
        },
        true, true, brls::Sound::SOUND_NONE);

        slider->getProgressEvent()->subscribe([this](float progress) {
            this->progress->setText(std::to_string((int)(progress * 100)));
        });
    }

    /** Increase brightness level
     * @return Increase brightness
     */
    bool PowerTab::brightnessUp()
    {
        brls::Logger::debug("Brightness UP");
        return brightness.brightnessUp();
    }

    /** Decrease brightness level
     * @return Decrease brightness
     */
    bool PowerTab::brightnessDown()
    {
        brls::Logger::debug("Brightness DOWN");
        return brightness.brightnessDown();
    }

    /** Get current birghtness level
     * @return Brightness level
     */
    int PowerTab::readBrightness()
    {
        return brightness.read();
    }

    brls::View *PowerTab::create()
    {
        return new PowerTab();
    }
}