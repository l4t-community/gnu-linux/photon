/*
    Borealis, a Nintendo Switch UI Library
    Copyright (C) 2021  natinusala

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "settings/settings.hpp"
#include "borealis/core/i18n.hpp"


namespace photon::activities
{
    bool close(brls::View *view) {
        brls::Application::popActivity();
        return true;
    }
    Settings::Settings() {
    }
    void Settings::onContentAvailable() {
        this->getContentView()->registerAction("Close", brls::BUTTON_B, close, true);;
    }
}
