#include "settings/settings.hpp"
#include "home/drawer.hpp"

const std::string drawer = R"xml(
<brls:Box
    width="auto"
    height="auto"
    axis="row"
    alignItems="center"
    justifyContent="spaceAround"
    marginBottom="33.3f">
    <brls:Image
        id="settings"
        image="res/img/settings.png"
        width="75px"
        height="75px"
        highlightCornerRadius="75px"
        cornerRadius="75px"
        focusable="true"/>
</brls:Box>
)xml";

namespace photon::views
{
    Drawer::Drawer()
    {
        this->inflateFromXMLString(drawer);

        this->settings->registerClickAction(launchSettings);
    }

    /** Push the settings activity to the View
     *  @param view The View
     *  @return true
    */
    bool Drawer::launchSettings(brls::View *view)
    {
        brls::Application::pushActivity(new photon::activities::Settings());
        return true;
    }

    brls::View* Drawer::create()
    {
        return new Drawer();
    }
}
