#include <string>
#include "home/carousel.hpp"

const std::string appCell = R"xml(
<brls:Box
	id="app"
	width="auto"
	height="auto"
	axis="column"
	alignItems="center">

	<brls:Label
		id="label"
		height = "auto"
		width = "auto"
		marginBottom = "10px"
		horizontalAlign="center"
		textColor="@theme/app_box/label"/>

	<brls:Box
		id="imgHolder"
		focusable="true"
		borderThickness="5px"
		width="220px"
		height="220px"
		shadowType="generic">
		<brls:Image
			id="image"
			width="auto"
			height="auto"/>
	</brls:Box>
</brls:Box>
)xml";

const std::string carousel = R"xml(
<brls:Box grow="1.0">
    <brls:HScrollingFrame scrollingBehavior="centered" grow="1.0">
        <brls:Box
            id="app"
            alignItems="center"
            height="auto"
            width="auto"
            paddingLeft="20px"
            paddingRight="20px"/>
    </brls:HScrollingFrame>
</brls:Box>
)xml";

bool hasEnding(std::string const &fullString, std::string const &ending)
{
    if (fullString.length() >= ending.length())
        return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    else return false;
}

namespace photon::views
{
    /* Constructs the XML element representing a single AppCell */
    AppCell::AppCell() {
        this->inflateFromXMLString(appCell);
        
        this->forwardXMLAttribute("scalingType", this->image);
        this->forwardXMLAttribute("image", this->image);
        this->forwardXMLAttribute("focusUp", this->image);
        this->forwardXMLAttribute("focusRight", this->image);
        this->forwardXMLAttribute("focusDown", this->image);
        this->forwardXMLAttribute("focusLeft", this->image);
        this->forwardXMLAttribute("imageWidth", this->image, "width");
        this->forwardXMLAttribute("imageHeight", this->image, "height");

        this->forwardXMLAttribute("caption", this->label, "text");

        this->forwardXMLAttribute("highlightCornerRadius", this->imgHolder);
        this->forwardXMLAttribute("cornerRadius", this->imgHolder);
    }

    void AppCell::onChildFocusGained(brls::View *directChild, brls::View *focusedView)
    {
        Box::onChildFocusGained(directChild, focusedView);
        this->label->show([]{});
    }

    void AppCell::onChildFocusLost(brls::View *directChild, brls::View *focusedView)
    {
        Box::onChildFocusLost(directChild, focusedView);
        this->label->hide([]{});
    }

    brls::View *AppCell::create()
    {
        return new AppCell();
    }
}

namespace photon::views
{
    /* Create the Carousel used in the home activity */
    Carousel::Carousel()
    {
        this->inflateFromXMLString(carousel);

        for (DesktopFilesList app : DesktopFiles::fetch())
            this->app->addView(createAppCellFromDesktopFile(app));

        this->app->addView(createAppCell("Add Item", NULL));
    }

    /** Instanciate a new unpopulated AppCell
     * @return An AppCell view
     */
    photon::views::AppCell *Carousel::newAppCell()
    {
        return new photon::views::AppCell();
    }
    
    /** Set an AppCell name/label
     * @param appCell The AppCell to modify
     * @param label The AppCell name
     */
    void Carousel::setAppCellLabel(photon::views::AppCell *appCell, const char *label)
    {
        appCell->label->setText(label);
        appCell->label->hide([]{});
    }

    /** Set an AppCell icon
     * @param appCell The AppCell to modify
     * @param iconPath The path to the desktop file icon
     */
    void Carousel::setAppCellIcon(photon::views::AppCell *appCell, const char *iconPath)
    {
        if (iconPath && !hasEnding(iconPath, ".svg"))
            appCell->image->setImageFromFile(iconPath);
    }

    /** Creates a new AppCell
     * @param label The AppCell name
     * @param iconPath The path to the desktop file icon
     * @return An AppCell view
     */
    photon::views::AppCell *Carousel::createAppCell(const char *label, const char *iconPath)
    {
        photon::views::AppCell *appCell = newAppCell();
        setAppCellLabel(appCell, label);
        setAppCellIcon(appCell, iconPath);
        return appCell;
    }

    /** Construct a desktop file representation
     * @param app A single DesktopFile representing the app
     * @return A constructed AppCell view with icon, name and action
     */
    photon::views::AppCell *Carousel::createAppCellFromDesktopFile(DesktopFilesList app)
    {
        photon::views::AppCell *appCell = createAppCell(app.name, app.iconPath);
        appCell->registerClickAction(
            [this, app](brls::View *view)
            {
                return DesktopFiles::launch(app.ref);
            }
        );
        return appCell;
    }
    
    brls::View *Carousel::create()
    {
        return new Carousel();
    }
}
