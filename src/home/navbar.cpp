#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>

#include "home/navbar.hpp"
#include "utils/mithraPlatform.hpp"

const std::string navbar = R"xml(
<brls:Box id="NavBar" axis="row">

<!-- <photon:CaptionedImage
        id = "profilePicture"
        image = "res/img/profilePicture.png"
        imageWidth = "75px" imageHeight = "75px"
        highlightCornerRadius = "75px"
        cornerRadius = "75px"
        focusable = "true"
        caption = "@i18n/home/userSettings"/> -->

    <brls:Box
        padding="20px"
        grow="1.0"
        width="auto"
        height="auto"
        axis="row"
        alignItems="center"
        direction="rightToLeft">

        <brls:Label id="brls/hints/time"/>
        <brls:Wireless id="brls/wireless" marginRight="21f"/>
        <brls:Battery id="brls/battery" marginRight="21f"/>

    </brls:Box>
</brls:Box>
)xml";

namespace photon::views
{
    NavBar::NavBar()
    {
        this->inflateFromXMLString(navbar);
 
        brls::Platform* platform = new photon::platform::MithraPlatform();
        this->battery->setVisibility(upower.isOnBattery() ? brls::Visibility::VISIBLE : brls::Visibility::GONE);
        this->wireless->setVisibility(platform->hasWirelessConnection() ? brls::Visibility::VISIBLE : brls::Visibility::GONE);
    }

    void NavBar::draw(NVGcontext* vg, float x, float y, float width, float height, brls::Style style, brls::FrameContext* ctx)
    {
        auto timeNow   = std::chrono::system_clock::now();
        auto in_time_t = std::chrono::system_clock::to_time_t(timeNow);

        std::stringstream ss;
        ss << std::put_time(std::localtime(&in_time_t), "%H:%M:%S");

        this->time->setText(ss.str());
        Box::draw(vg, x, y, width, height, style, ctx);
    }

    brls::View* NavBar::create()
    {
        return new NavBar();
    }
}