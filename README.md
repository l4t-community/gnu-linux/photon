# Photon

Photon is borealis based app launcher designed for Linux.

## Build

Fedora :
```sh
sudo dnf install -y meson ninja-build cmake glfw-devel glm-devel g++ glib2-devel gtk3-devel
```

```sh
git clone https://gitlab.com/l4t-community/gnu-linux/photon
cd photon
meson build
ninja -j4 -C build
```
