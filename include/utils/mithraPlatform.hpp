#pragma once

#include <borealis/platforms/glfw/glfw_platform.hpp>

#include "upower.hpp"

class UPower;

namespace photon::platform
{
    class MithraPlatform : public brls::GLFWPlatform
    {
        public:
            MithraPlatform();

            int getBatteryLevel() override;
            bool hasWirelessConnection() override;
            // int getWirelessLevel() override;
            // bool isBatteryCharging() override;
            // bool canShowBatteryLevel() override;

        private:
            UPower upower;
    };
}