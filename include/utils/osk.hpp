#pragma once

#include <map>
#include <string>

#include <borealis/core/box.hpp>
#include <borealis/core/input.hpp>
#include <borealis/views/button.hpp>
#include <borealis/views/rectangle.hpp>

namespace photon::utils {
    class brls::View;

    typedef std::function<bool(brls::View*)> ActionListener;
    typedef int ActionIdentifier;

    #define ACTION_NONE -1

    struct Action
    {
        brls::BrlsKeyboardScancode button;

        ActionIdentifier identifier;
        std::string hintText;
        bool available;
        bool allowRepeating;
        enum brls::Sound sound;
        ActionListener actionListener;

        bool operator==(const brls::BrlsKeyboardScancode other)
        {
            return this->button == other;
        }
    };

    class Osk : public brls::Box {
        public:
            Osk();
            static brls::View *create();

            void showOsk(brls::View *view);
    };

    class Key : public brls::Button {
        public:
            Key();
            Key(std::string name,
                brls::BrlsKeyboardScancode keycode,
                float sizeHint = 1.0f);
            static brls::View *create();
        
        private:
            std::vector<Action> actions;

            ActionIdentifier registerAction(std::string hintText,
                brls::BrlsKeyboardScancode button,
                ActionListener actionListener,
                bool allowRepeating,
                enum brls::Sound sound);
    };
}