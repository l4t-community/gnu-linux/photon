#pragma once

#include <borealis.hpp>

#include "upower.hpp"

namespace photon::views
{
        class NavBar : public brls::Box
    {
        public:
            NavBar();
            static brls::View* create();

            void draw(NVGcontext* vg, float x, float y, float width, float height, brls::Style style, brls::FrameContext* ctx) override;

        private:
            UPower upower;

            BRLS_BIND(brls::Label, time, "brls/hints/time");
            BRLS_BIND(brls::View, battery, "brls/battery");
            BRLS_BIND(brls::View, wireless, "brls/wireless");
    };
}