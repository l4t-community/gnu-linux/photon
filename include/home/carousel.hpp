#pragma once

#include <borealis.hpp>

#include "desktop_files.hpp"

namespace photon::views
{
    class AppCell : public brls::Box
    {
        public:
            AppCell();

            static brls::View *create();

            void onChildFocusLost(View *directChild, View *focusedView) override;
            void onChildFocusGained(View *directChild, View *focusedView) override;

            BRLS_BIND(brls::Image, image, "image");
            BRLS_BIND(brls::Label, label, "label");
            BRLS_BIND(brls::Box, imgHolder, "imgHolder");
    };

    class Carousel : public brls::Box
    {
        public:
            Carousel();
            static brls::View *create();

            void setAppCellLabel(photon::views::AppCell *appCell, const char *label);
            void setAppCellIcon(photon::views::AppCell *appCell, const char *iconPath);

            photon::views::AppCell *newAppCell();
            photon::views::AppCell *createAppCell(const char *label, const char *iconPath);
            photon::views::AppCell *createAppCellFromDesktopFile(DesktopFilesList app);

        private:
            BRLS_BIND(brls::Box, app, "app");
    };
}