#pragma once

#include <borealis.hpp>

namespace photon::views
{   
    class Drawer : public brls::Box
    {
        public:
            Drawer();
            static brls::View* create();

        private:
            static bool launchSettings(brls::View *view);
            BRLS_BIND(brls::Image, settings, "settings");
    };
}
