#pragma once

#include <borealis.hpp>

namespace photon::activities
{
    class InitialSetup : public brls::Activity
    {
        public:
            InitialSetup();
            CONTENT_FROM_XML_RES("initialSetup/initialsetup.xml");
    };
}