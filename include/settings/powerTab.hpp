#pragma once

#include <borealis.hpp>

#include "brightness.hpp"
#include "upower.hpp"

namespace photon::tabs
{
    class PowerTab : public brls::Box
    {
        public:
            PowerTab();
            static brls::View *create();

        private:
            UPower upower;
            Brightness brightness;

            bool brightnessUp();
            bool brightnessDown();
            void setBrightnessLevel();
            int readBrightness();

            BRLS_BIND(brls::Label, progress, "progress");
            BRLS_BIND(brls::Slider, slider, "brightness");
    };
}