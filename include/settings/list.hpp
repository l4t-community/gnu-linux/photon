#pragma once

#include <borealis.hpp>

namespace photon::views
{
    class List : public brls::Box
    {
        public:
            List();
            static brls::View *create();

        private:
            BRLS_BIND(brls::Image, image, "image");
            BRLS_BIND(brls::Label, label, "label");
    };
}