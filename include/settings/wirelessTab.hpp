#pragma once

#include <borealis.hpp>

#include "bluetooth.hpp"

namespace photon::tabs
{
    class WirelessTab : public brls::Box
    {
        public:
            WirelessTab();
            static brls::View *create();

        private:
            BlueZ bluez;

            bool onWifiButtonClicked(brls::View *view);
            bool onBluetoothButtonClicked(brls::View *view);
            std::vector<std::string> ListAccessPoints();
            std::vector<std::string> ListBluetoothDevices();
    };
}